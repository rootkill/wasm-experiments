## Site

After we bundle our WASM package into npm package, we make use of that bundle in our javascript web application.

We make use of webpack bundle from npm to serve this application.

#### To run this JavaScript web application

Use the command:

```bash
npm run serve
```

Then you can see the WebAssembly package run in Browser Alert when you go to `localhost:8080` in action.

Pretty Cool Right? Well there's more to come! Stay Tuned! 😉
