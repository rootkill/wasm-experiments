# WASM Streaming Experiment

Since we are able to call WASM functions using JavaScript,
Let's try and take it to another level by modifying DOM Elements using WASM Streaming functions!

Code in the module (or directory or ...) explains how we can call the WASM Function from a .wasm file using JavaScript and updating the value of DOM Element with the values provided by the WASM module

To test this code, you can start a basic HTTP server in this Directory using: `python3 -m http.server` comand and after the server is up and running you can go to `localhost:8000` in your browser and interact with the application.

The appliation also logs on the console apart from manipulating the DOM Element so you can also check that in Developer Console (You may need to refresh to see the button activity in the console).
