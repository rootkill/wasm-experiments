# wasm-experiments

Here in this repository I am going to play around with WASM (and possibly gRPC and Protocol Buffers as well).

The program currently in this repository is very basic and uses JS function `alert` and call `greet` from rust code 
and prompts the user about welcome message using browser alert!

You can use basic server to serve the index.html file, for example with Python:

```bash
python3 -m http.server
```

and then you can see the interoperability of JS and Rust WASM in action by going to http://localhost:8000
